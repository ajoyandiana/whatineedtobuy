package com.example.listt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var next = 1
    var res = 0
    var sum1: Float = 0F
    var sum2: Float = 0F
    var sum3: Float = 0F
    var sum4: Float = 0F
    var sum5: Float = 0F
    var sum6: Float = 0F
    var sum7: Float = 0F
    var sum8: Float = 0F
    var sum9: Float = 0F
    var sum: Float = 0F
    var one: Float = 0F
    var two: Float = 0F
    var three: Float = 0F
    var four: Float = 0F
    var five: Float = 0F
    var six: Float = 0F
    var seven: Float = 0F
    var eight: Float = 0F
    var nine: Float = 0F


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        plusButton.setOnClickListener {
            plus()
        }

        minusButton.setOnClickListener {
            minus()
        }

        addButton.setOnClickListener {
            addText()
            editDelete()

        }
        text1.setOnClickListener {

        }
        number.setOnClickListener {

        }
        editText.setOnClickListener {

        }
        delete1.setOnClickListener {
            deleteOne()
        }
        delete2.setOnClickListener {
            deleteTwo()
        }
        delete3.setOnClickListener {
            deleteThree()
        }
        delete4.setOnClickListener {
            deleteFour()
        }
        delete5.setOnClickListener {
            deleteFive()
        }
        delete6.setOnClickListener {
            deleteSix()
        }
        delete7.setOnClickListener {
            deleteSeven()
        }
        delete8.setOnClickListener {
            deleteEight()
        }
        delete9.setOnClickListener {
            deleteNine()
        }
        sumButton.setOnClickListener {
            sum()

        }
        deleteSum.setOnClickListener {
            delete()

        }
        sumOne.setOnClickListener {
            deleteOne()
        }
        sumTwo.setOnClickListener {
            deleteTwo()
        }
        sumThree.setOnClickListener {
            deleteThree()
        }
        sumFour.setOnClickListener {
            deleteFour()
        }
        sumFive.setOnClickListener {
            deleteFive()
        }
        sumSix.setOnClickListener {
            deleteSix()
        }
        sumSeven.setOnClickListener {
            deleteSeven()
        }
        sumEight.setOnClickListener {
            deleteEight()
        }
        sumNine.setOnClickListener {
            deleteNine()
        }

    }

    private fun addText() {
        if (text1.text.isEmpty() && count1.text.isEmpty()) {
            text1.text = editText.text.toString()
            count1.text = number.text.toString()
        } else if (text1.text.isNotEmpty() && text2.text.isEmpty()) {
            text2.text = editText.text.toString()
            count2.text = number.text.toString()
        } else if (text2.text.isNotEmpty() && text3.text.isEmpty()) {
            text3.text = editText.text.toString()
            count3.text = number.text.toString()
        } else if (text3.text.isNotEmpty() && text4.text.isEmpty()) {
            text4.text = editText.text.toString()
            count4.text = number.text.toString()
        } else if (text4.text.isNotEmpty() && text5.text.isEmpty()) {
            text5.text = editText.text.toString()
            count5.text = number.text.toString()
        } else if (text5.text.isNotEmpty() && text6.text.isEmpty()) {
            text6.text = editText.text.toString()
            count6.text = number.text.toString()
        } else if (text6.text.isNotEmpty() && text7.text.isEmpty()) {
            text7.text = editText.text.toString()
            count7.text = number.text.toString()
        } else if (text7.text.isNotEmpty() && text8.text.isEmpty()) {
            text8.text = editText.text.toString()
            count8.text = number.text.toString()
        } else if (text8.text.isNotEmpty() && text9.text.isEmpty()) {
            text9.text = editText.text.toString()
            count9.text = number.text.toString()
        }
    }

    private fun plus() {
        val num = number.text.toString().toInt()
        res = num + next
        number.text = res.toString()
    }

    private fun minus() {
        val num = number.text.toString().toInt()
        res = num - next
        number.text = res.toString()
    }

    private fun deleteOne() {
        text1.text = ""
        count1.text = ""
        sum = sum - (sum1 * one)
        result.text = sum.toString()
        sumOne.getText().clear()
    }


    private fun deleteTwo() {
        text2.text = ""
        count2.text = ""
        sum = sum - (sum2 * two)
        result.text = sum.toString()
        sumTwo.getText().clear()

    }

    private fun deleteThree() {
        text3.text = ""
        count3.text = ""
        sum = sum - (sum3 * three)
        result.text = sum.toString()
        sumThree.getText().clear()

    }

    private fun deleteFour() {
        text4.text = ""
        count4.text = ""
        sum = sum - (sum4 * four)
        result.text = sum.toString()
        sumFour.getText().clear()
    }

    private fun deleteFive() {
        text5.text = ""
        count5.text = ""
        sum = sum - (sum5 * five)
        result.text = sum.toString()
        sumFive.getText().clear()

    }

    private fun deleteSix() {
        text6.text = ""
        count6.text = ""
        sum = sum - (sum6 * six)
        result.text = sum.toString()
        sumSix.getText().clear()

    }

    private fun deleteSeven() {
        text7.text = ""
        count7.text = ""
        sum = sum - (sum7 * seven)
        result.text = sum.toString()
        sumSeven.getText().clear()
    }

    private fun deleteEight() {
        text8.text = ""
        count8.text = ""
        sum = sum - (sum8 * eight)
        result.text = sum.toString()
        sumEight.getText().clear()
    }

    private fun deleteNine() {
        text9.text = ""
        count9.text = ""
        sum = sum - (sum9 * nine)
        result.text = sum.toString()
        sumNine.getText().clear()
    }

    private fun delete() {
        text1.text = ""
        text2.text = ""
        text3.text = ""
        text4.text = ""
        text5.text = ""
        text6.text = ""
        sum1 = 0F
        sum2 = 0F
        sum3 = 0F
        sum4 = 0F
        sum5 = 0F
        sum6 = 0F
        sum7 = 0F
        sum8 = 0F
        sum9 = 0F
        result.text = ""
        count1.text = ""
        count2.text = ""
        count3.text = ""
        count4.text = ""
        count5.text = ""
        count6.text = ""
        count7.text = ""
        count8.text = ""
        count9.text = ""
        one = 0F
        two = 0F
        three = 0F
        four = 0F
        five = 0F
        six = 0F
        seven = 0F
        eight = 0F
        nine = 0F
        sumOne.getText().clear()
        sumTwo.getText().clear()
        sumThree.getText().clear()
        sumFour.getText().clear()
        sumFive.getText().clear()
        sumSix.getText().clear()
        sumSeven.getText().clear()
        sumEight.getText().clear()
        sumNine.getText().clear()


    }

    private fun editDelete() {
        editText.getText().clear()
    }

    private fun sum() {
        if (count1.text.isNotEmpty()) {
            one = count1.text.toString().toFloat()
        }
        if (count2.text.isNotEmpty()) {
            two = count2.text.toString().toFloat()
        }
        if (count3.text.isNotEmpty()) {
            three = count3.text.toString().toFloat()
        }
        if ((count4.text.isNotEmpty())) {
            four = count4.text.toString().toFloat()
        }
        if (count5.text.isNotEmpty()) {
            five = count5.text.toString().toFloat()
        }
        if (count6.text.isNotEmpty()) {
            six = count6.text.toString().toFloat()
        }
        if (count7.text.isNotEmpty()) {
            seven = count7.text.toString().toFloat()
        }
        if (count8.text.isNotEmpty()) {
            eight = count8.text.toString().toFloat()
        }
        if (count9.text.isNotEmpty()) {
            nine = count9.text.toString().toFloat()
        }


        if (sumOne.text.isNotEmpty()) {
            sum1 = sumOne.text.toString().toFloat()
        }
        if (sumTwo.text.isNotEmpty()) {
            sum2 = sumTwo.text.toString().toFloat()
        }
        if (sumThree.text.isNotEmpty()) {
            sum3 = sumThree.text.toString().toFloat()
        }
        if (sumFour.text.isNotEmpty()) {
            sum4 = sumFour.text.toString().toFloat()
        }
        if (sumFive.text.isNotEmpty()) {
            sum5 = sumFive.text.toString().toFloat()
        }
        if (sumSix.text.isNotEmpty()) {
            sum6 = sumSix.text.toString().toFloat()
        }
        if (sumSeven.text.isNotEmpty()) {
            sum7 = sumSeven.text.toString().toFloat()
        }
        if (sumEight.text.isNotEmpty()) {
            sum8 = sumEight.text.toString().toFloat()
        }
        if (sumNine.text.isNotEmpty()) {
            sum9 = sumNine.text.toString().toFloat()
        }

        val c1 = sum1 * one
        val c2 = sum2 * two
        val c3 = sum3 * three
        val c4 = sum4 * four
        val c5 = sum5 * five
        val c6 = sum6 * six
        val c7 = sum7 * seven
        val c8 = sum8 * eight
        val c9 = sum9 * nine
        sum = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9
        result.text = sum.toString()


    }

}